package tech.dnene.squall

import arrow.core.*
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.statements.InsertStatement
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import org.jetbrains.exposed.sql.statements.UpdateStatement


interface Identifiable<I : Comparable<I>> {
    val id: I
}

interface Entity
interface IdEntity<I : Comparable<I>> : Identifiable<I>, Entity

interface Persister<E : Any, T : Table> {
    val baseTable: T
    fun serialise(e: E): T.(UpdateBuilder<Int>) -> Unit
    fun save(e: E, cascadeDependents: Boolean = true): Either<DatabaseError, InsertStatement<Number>> = try {
        baseTable.insert(serialise(e)).right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object",
            mapOf("class" to e.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }

    fun save(list: List<E>, cascadeDependents: Boolean = true): Either<DatabaseError, Unit> = try {
        baseTable.batchInsert<T, E>(list) { e ->
            serialise(e)(baseTable, this)
        }
        Unit.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object",
            kotlin.collections.mapOf("class" to list.first().javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }

    fun update(e: E, cascadeDependents: Boolean=true): Either<DatabaseError, Int> = try {
        baseTable.update(null, null, serialise(e)).right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-updating-object",
            kotlin.collections.mapOf("class" to e.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }
}

interface DepPersister<EP : Any, E : Any, T : Table> {
    val baseTable: T
    val accessor: (EP) -> List<E>
    fun serialise(parent: EP, e: E): T.(UpdateBuilder<Int>) -> Unit
    fun save(parent: EP, cascadeDependents: Boolean = true): Either<DatabaseError, Unit> = try {
        val details = accessor(parent)
        baseTable.batchInsert<T, E>(accessor(parent)) { e ->
            serialise(parent, e)(baseTable, this)
        }
        Unit.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object-tree",
            mapOf("parent-class" to parent.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }

    fun save(parents: List<EP>, cascadeDependents: Boolean = true): Either<DatabaseError, Unit> = try {
        val details = parents.flatMap { parent -> accessor(parent).map { parent to it } }
        baseTable.batchInsert<T, Pair<EP, E>>(details) { parentChildPair ->
            serialise(parentChildPair.first, parentChildPair.second)(baseTable, this)
        }
        Unit.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object-tree",
            kotlin.collections.mapOf("parent-class" to parents.first().javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }
    fun update(parent: EP, cascadeDependents: Boolean): Either<DatabaseError, Int> = try {
        // todo : fix .. this will need an ID Table
        0.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-updating-object-tree",
            kotlin.collections.mapOf("parent-class" to parent.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }
}

interface Loader<E> {
    val baseTable: FieldSet
    fun deserialise(row: ResultRow): Either<DatabaseError, E>
    private fun Query.fold(): Either<DatabaseError, List<E>> =
        fold(mutableListOf<E>().right() as Either<DatabaseError, MutableList<E>>) { acc, elem ->
            acc.flatMap { list: MutableList<E> ->
                deserialise(elem).flatMap { e: E ->
                    list.add(e); list.right()
                }
            }
        }

    fun selectAll() = try {
        baseTable.selectAll().fold()
    } catch (ex: Exception) {
        DatabaseError(
            "err-querying-database",
            kotlin.collections.mapOf(),
            ex.some()
        ).left()
    }

    fun select(where: SqlExpressionBuilder.() -> Op<Boolean>) = try {
        baseTable.select(where).fold()
    } catch (ex: Exception) {
        DatabaseError(
            "err-querying-database",
            kotlin.collections.mapOf(),
            ex.some()
        ).left()
    }

    fun selectOne(where: SqlExpressionBuilder.() -> Op<Boolean>): Either<DatabaseError, E> = try {
        baseTable.select(where).let {
            when (it.count()) {
                0 -> DatabaseError("err-no-rows-found", Fault.EMPTY_MAP, None).left()
                1 -> deserialise(it.first())
                else -> DatabaseError("err-too-many-rows-found", Fault.EMPTY_MAP, None).left()
            }
        }
    } catch (ex: Exception) {
        DatabaseError(
            "err-querying-database",
            kotlin.collections.mapOf(),
            ex.some()
        ).left()
    }
}

interface Serialiser<E : Any, T : Table> : Persister<E, T>, Loader<E>
interface DepSerialiser<EP : Any, E : Any, T : Table> : DepPersister<EP, E, T>, Loader<E>

interface IdPersister<I : Comparable<I>, E : IdEntity<I>, T : IdTable<I>> : Persister<E, T> {
    val dependentPersisters: List<DepPersister<E, *, *>>
    override fun save(e: E, cascadeDependents: Boolean): Either<DatabaseError, InsertStatement<Number>> = try {
        val stmt = baseTable.insert(serialise(e))
        if (cascadeDependents) {
            dependentPersisters.fold(Unit.right() as Either<DatabaseError, Unit>) { acc, elem ->
                acc.flatMap { elem.save(e) }
            }.flatMap { stmt.right() }
        } else stmt.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object",
            mapOf("class" to e.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }

    override fun save(list: List<E>, cascadeDependents: Boolean): Either<DatabaseError, Unit> = try {
        baseTable.batchInsert<T, E>(list) { e ->
            serialise(e)(baseTable, this)
        }
        if (cascadeDependents) {
            dependentPersisters.fold(Unit.right() as Either<DatabaseError, Unit>) { acc, elem ->
                acc.flatMap { elem.save(list) }
            }
        }
        Unit.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object",
            mapOf("class" to list.first().javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }

    override fun update(e: E, cascadeDependents: Boolean): Either<DatabaseError, Int> = try {
        val updated = baseTable.update({ baseTable.id eq e.id }, null, serialise(e))
        if (cascadeDependents) {
            dependentPersisters.fold(Unit.right() as Either<DatabaseError, Unit>) { acc, elem ->
                acc.flatMap { elem.update(e, cascadeDependents); Unit.right() }
            }.flatMap { updated.right() }
        } else updated.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-saving-object",
            mapOf("class" to e.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }

    fun delete(e: E, cascadeDependents: Boolean = true): Either<DatabaseError, Unit> = try {
        //todo: cascade delete
        baseTable.deleteWhere { baseTable.id eq e.id }
        Unit.right()
    } catch (ex: Exception) {
        DatabaseError(
            "err-delete-object",
            mapOf("class" to e.javaClass, "table" to baseTable.tableName),
            ex.some()
        ).left()
    }
}

interface IdLoader<I : Comparable<I>, E : IdEntity<I>> : Loader<E>

interface IdSerialiser<I : Comparable<I>, E : IdEntity<I>, T : IdTable<I>> : IdPersister<I, E, T>, IdLoader<I, E> {
    fun selectById(id: I) = selectOne { baseTable.id eq id }
}

abstract class AbstractIdSerialiser<I : Comparable<I>, E : IdEntity<I>, T : IdTable<I>> : IdSerialiser<I, E, T> {
    override val dependentPersisters: List<DepPersister<E, *, *>> = emptyList()
}