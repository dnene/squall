package tech.dnene.squall

import arrow.core.None
import arrow.core.Option
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.slf4j.Logger
import org.slf4j.LoggerFactory

inline fun <reified T> logger(): Logger = LoggerFactory.getLogger(T::class.java)

abstract class Fault(
    val message: String,
    val args: Map<String, Any?>,
    val cause: Option<Throwable>
) {
    companion object {
        val EMPTY_MAP = emptyMap<String, Any?>()
    }

    override fun toString() = "${javaClass.name}(${message}:${args}:${cause})"
    override fun equals(other: Any?): Boolean =
        (other as? Fault)?.let {
            message == it.message &&
                    args == it.args &&
                    cause == it.cause
        } ?: false
}

fun connect(database: String, username: String, password: String, maximumPoolSize: Int): Database {
    val dataSource = HikariDataSource().apply {
        jdbcUrl = "jdbc:mysql://localhost:3306/${database}?useSSL=false"
        this.username = username
        this.password = password
        this.maximumPoolSize = maximumPoolSize
        isAutoCommit = false
    }

    return Database.connect(dataSource)
}

open class DatabaseError(
    message: String,
    args: Map<String, Any?>,
    cause: Option<Throwable> = None
) : Fault(message, args, cause)
