package tech.dnene.squall

import arrow.core.left
import arrow.core.right
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

object HiLoOidBlocks: Table("oids") {
    val key = varchar("handle", 32)
    val rangeBits = short("range_bits")
    val hiCounter = long("hi_counter")
}

class HiLoOidGenerator(val db: Database, val key: String, val hiValue: Long, val rangeBits: Short, val currentLow: AtomicLong = AtomicLong(0)) {
    companion object {
        val keyBlocksMap = ConcurrentHashMap<String, HiLoOidGenerator>()
        fun getNextBlock(db: Database, pKey: String, rangeBits: Short): HiLoOidGenerator {
            synchronized(pKey.intern()) {
                return transaction(db) {
                    val row = HiLoOidBlocks.select { HiLoOidBlocks.key.eq(pKey) }.firstOrNull()
                    if (row == null) {
                        HiLoOidBlocks.insert {
                            it[key] = pKey
                            it[this.rangeBits] = rangeBits
                            it[hiCounter] = 0
                        }
                        HiLoOidGenerator(db, pKey, 0, rangeBits).apply {
                            keyBlocksMap[pKey] = this
                        }

                    } else {
                        val newCounter = row[HiLoOidBlocks.hiCounter] + 1
                        val bits = row[HiLoOidBlocks.rangeBits]
                        HiLoOidBlocks.update({ HiLoOidBlocks.key.eq(pKey) }) {
                            it[HiLoOidBlocks.hiCounter] = newCounter
                            it[HiLoOidBlocks.rangeBits] = rangeBits
                        }
                        HiLoOidGenerator(db, pKey, newCounter, rangeBits).apply {
                            keyBlocksMap[pKey] = this
                        }
                    }
                }
            }
        }
        fun next(db: Database, key: Table, rangeBits: Short) = next(db, key.tableName, rangeBits)
        fun next(db: Database, key: String, rangeBits: Short) = keyBlocksMap.getOrPut(key, { getNextBlock(db, key, rangeBits) }).nextValue()
    }
    val range = 1L shl rangeBits.toInt()
    val hiComputed = hiValue shl rangeBits.toInt()
    fun nextValue(): Long {
        // todo: is this locking good enough?
        val result = synchronized(key.intern()) {
            val cur = currentLow.incrementAndGet()
            if (cur <= range) {
                (hiComputed + cur).right()
            } else {
                getNextBlock(db, key, rangeBits).left()
            }
        }
        return result.fold({it.nextValue()}, {it})
    }
}

fun IdTable<Long>.next(db: Database, rangeBits: Short = 8) = HiLoOidGenerator.next(db, tableName, rangeBits)