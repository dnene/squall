package tech.dnene.squall.sampleschemas.person

import arrow.core.Either
import arrow.core.None
import arrow.core.extensions.fx
import arrow.core.left
import arrow.core.right
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import tech.dnene.squall.DatabaseError
import java.time.LocalDate

class TestPerson : TestBase() {
    companion object {
        val log = LoggerFactory.getLogger(TestPerson::class.java)

        @BeforeAll
        @JvmStatic
        fun preloads() {
            recreateTables()
            transaction {
                PersonSerialiser.save(
                    Person(
                        1,
                        "foo",
                        "bar",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1)
                    )
                )
                PersonSerialiser.save(
                    Person(
                        2,
                        "buz",
                        "baz",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1)
                    )
                )
                PersonSerialiser.save(
                    Person(
                        0,
                        "buz",
                        "baz",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1)
                    )
                )
            }
        }
    }


    @Test
    fun testSelectSome() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val persons = !PersonSerialiser.select { PersonsTable.id greaterEq 1L }
                assertEquals(2, persons.size, "Only two rows should've been returned")
                Unit.right()
            }.mapLeft { fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectAll() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val persons = !PersonSerialiser.selectAll()
                assertEquals(3, persons.size, "There should be three persons in the table")
                Unit.right()
            }.mapLeft { fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectOneWithZeroResults() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                !PersonSerialiser.selectOne { PersonsTable.id eq 9L }
                Unit.right()
            }.mapLeft { assertEquals(DatabaseError("err-no-rows-found", emptyMap(), None), it); it.left() }
        }
    }

    @Test
    fun testSelectOneWithOneResults() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val person1 = !PersonSerialiser.selectOne { PersonsTable.id eq 1L }
                assertEquals(1L, person1.id, "Person should've id 1")
                assertEquals("foo", person1.givenName, "Person first name is not correct")
                Unit.right()
            }.mapLeft { fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectOneWithMultipleResults() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                !PersonSerialiser.selectOne { PersonsTable.id greaterEq 1L }
                Unit.right()
            }.mapLeft { assertEquals(DatabaseError("err-too-many-rows-found", emptyMap(), None), it); it.left() }
        }
    }

    @Test
    fun testSelectOneById() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val person1 = !PersonSerialiser.selectById(1L)
                assertEquals(1L, person1.id, "Person should've id 1")
                assertEquals("foo", person1.givenName, "Person first name is not correct")
                Unit.right()
            }.mapLeft { fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testUpdate() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val person1 = !PersonSerialiser.selectById(1L)
                assertEquals(1L, person1.id, "Person should've id 1")
                assertEquals("foo", person1.givenName, "Person first name is not correct")
                val personChanged  = person1.copy(givenName = "bla bla")
                PersonSerialiser.update(personChanged)
                Unit.right()
            }.mapLeft { fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }
}