package tech.dnene.squall.sampleschemas.person

import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import tech.dnene.squall.connect

open class TestBase {
    companion object {
        val log = LoggerFactory.getLogger(TestPerson::class.java)
        val tables = listOf(PersonsTable to true, ContactsTable to true)
        val db = connect("squall", "squall", "squall", 3)

        fun recreateTables() {
            transaction {
                tables.reversed().forEach {
                    try {
                        if (it.second) SchemaUtils.drop(it.first)
                    } catch (e: Exception) {
                        log.error("err-unable-to-drop-table-${it.first}", e)
                    }
                }

                tables.forEach {
                    SchemaUtils.create(it.first)
                }
            }
        }

        fun cleanTables() {
            transaction {
                tables.reversed().forEach {
                    if (it.second) it.first.deleteAll()
                }
            }
        }
    }

}