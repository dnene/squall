package tech.dnene.squall.sampleschemas.person

import arrow.core.Either
import arrow.core.extensions.fx
import arrow.core.left
import arrow.core.right
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import tech.dnene.squall.DatabaseError
import java.time.LocalDate


class TestPersonContacts : TestBase() {
    companion object {
        val log = LoggerFactory.getLogger(TestPersonContacts::class.java)

        @BeforeAll
        @JvmStatic
        fun preloads() {
            recreateTables()
        }
    }

    @BeforeEach
    fun resetTables() {
        cleanTables()
        transaction {
            PersonTreeSerialiser.save(
                PersonTree(
                    1,
                    "foo",
                    "bar",
                    Sex.Male,
                    LocalDate.of(2020, 1, 1),
                    listOf(Contact(ContactType.Email, "foo@bar.com"), Contact(ContactType.Mobile, "+91 999 555 1212"))
                )
            )

            PersonTreeSerialiser.save(
                listOf(
                    PersonTree(
                        2,
                        "buz",
                        "baz",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1),
                        listOf(
                            Contact(ContactType.Email, "buz@baz.com"),
                            Contact(ContactType.Mobile, "+91 999 555 1313")
                        )
                    ),
                    PersonTree(
                        3,
                        "goo",
                        "zar",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1),
                        listOf(
                            Contact(ContactType.Email, "goo@zar.com"),
                            Contact(ContactType.Mobile, "+91 999 555 1414")
                        )
                    )
                )
            )
//
////                PersonSerialiser.save(
////                    Person(
////                        1,
////                        "foo",
////                        "bar",
////                        Sex.Male,
////                        LocalDate.of(2020, 1, 1)
////                    )
////                )
////                ContactsSerialiserWithPersonId.save(PersonContact(1, ContactType.Email, "foo@bar.com"))
////                ContactsSerialiserWithPersonId.save(PersonContact(1, ContactType.Mobile, "+91 999 555 1212"))
//            PersonSerialiser.save(
//                Person(
//                    2,
//                    "buz",
//                    "baz",
//                    Sex.Male,
//                    LocalDate.of(2020, 1, 1)
//                )
//            )
//            ContactsSerialiserWithPersonId.save(PersonContact(2, ContactType.Email, "buz@baz.com"))
//            ContactsSerialiserWithPersonId.save(PersonContact(2, ContactType.Mobile, "+91 999 666 1212"))
//            PersonSerialiser.save(
//                Person(
//                    0,
//                    "buz",
//                    "baz",
//                    Sex.Male,
//                    LocalDate.of(2020, 1, 1)
//                )
//            )
        }
    }


    @Test
    fun testSelectOneWithOneResult() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val person1 = !PersonTreeSerialiser.selectOne { PersonsTable.id eq 1L }
                println(person1)
//                assertEquals(1L, person1.id, "Person should've id 1")
//                assertEquals("foo", person1.givenName, "Person first name is not correct")
//                assertEquals(0, person1.contacts.size)
//                val contacts = !ContactsSerialiserWithPersonId.select { ContactsTable.personId eq person1.id }
//                assertEquals(2, contacts.size)
//                val person2 = person1.copy(contacts = contacts.map { Contact(it.type, it.text)})
//
//                val person3 = person2.copy(id = 99)
//                !PersonContactsSerialiser.save(person3)
                Unit.right()
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectById() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val person1 = !PersonTreeSerialiser.selectById(1L)
                println(person1)
//                assertEquals(1L, person1.id, "Person should've id 1")
//                assertEquals("foo", person1.givenName, "Person first name is not correct")
//                assertEquals(0, person1.contacts.size)
//                val contacts = !ContactsSerialiserWithPersonId.select { ContactsTable.personId eq person1.id }
//                assertEquals(2, contacts.size)
//                val person2 = person1.copy(contacts = contacts.map { Contact(it.type, it.text)})
//
//                val person3 = person2.copy(id = 99)
//                !PersonContactsSerialiser.save(person3)
                Unit.right()
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }
}