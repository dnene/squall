package tech.dnene.squall.sampleschemas.person

import arrow.core.Either
import arrow.core.None
import arrow.core.extensions.fx
import arrow.core.left
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.platform.commons.logging.LoggerFactory
import tech.dnene.squall.DatabaseError
import java.time.LocalDate

class TestContacts : TestBase() {
    companion object {
        val log = LoggerFactory.getLogger(TestBase::class.java)

        @BeforeAll
        @JvmStatic
        fun preloads() {
            recreateTables()
        }
    }

    @BeforeEach
    fun resetTables() {
        println("Cleaning tables")
        cleanTables()
        println("Prepopulating tables")
        transaction {
            PersonSerialiser.save(
                Person(
                    1,
                    "foo",
                    "bar",
                    Sex.Male,
                    LocalDate.of(2020, 1, 1)
                )
            )
            ContactsSerialiser.save(PersonContact(1, ContactType.Email, "foo@bar.com"))
            ContactsSerialiser.save(PersonContact(1, ContactType.Mobile, "+91 999 555 1212"))
        }
    }


    @Test
    fun testDependentBatchInsert() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                !PersonSerialiser.save(
                    Person(
                        2,
                        "buz",
                        "baz",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1)
                    )
                )
                val person = !PersonSerialiser.selectById(2)
                val contacts = listOf(
                    Contact(ContactType.Email, "boo@bar.com"),
                    Contact(ContactType.Mobile, "+91 888 555 1212")
                )
                DepContactsSerialiser.save(PersonTree.from(person, contacts))
                val retrievedContacts =
                    (!DepContactsSerialiser.select { ContactsTable.personId eq 2L }).sortedBy { it.text }
                assertEquals(2, retrievedContacts.size, "Number of contacts mismatch")
                assertEquals(Contact(ContactType.Mobile, "+91 888 555 1212"), retrievedContacts[0])
                assertEquals(Contact(ContactType.Email, "boo@bar.com"), retrievedContacts[1])
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testIndependentBatchInsert() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                !PersonSerialiser.save(
                    Person(
                        3,
                        "buz",
                        "baz",
                        Sex.Male,
                        LocalDate.of(2020, 1, 1)
                    )
                )
                val contacts = listOf(
                    PersonContact(3L, ContactType.Email, "boo@bar.com"),
                    PersonContact(3L, ContactType.Mobile, "+91 888 555 1212")
                )
                !ContactsSerialiser.save(contacts)
                val retrievedContacts =
                    (!ContactsSerialiser.select { ContactsTable.personId eq 3L }).sortedBy { it.text }
                assertEquals(2, retrievedContacts.size, "Number of contacts mismatch")
                assertEquals(PersonContact(3L, ContactType.Mobile, "+91 888 555 1212"), retrievedContacts[0])
                assertEquals(PersonContact(3L, ContactType.Email, "boo@bar.com"), retrievedContacts[1])
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectAll() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val contacts = !ContactsSerialiserWithPersonId.selectAll()
                assertEquals(2, contacts.size, "Expected 2 contacts")
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectOneZeroResults() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                !ContactsSerialiserWithPersonId.selectOne { ContactsTable.type eq ContactType.Address }
            }.mapLeft { assertEquals(DatabaseError("err-no-rows-found", emptyMap(), None), it); it.left() }
        }
    }

    @Test
    fun testSelectOneWithOneResult() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val contact = !ContactsSerialiserWithPersonId.selectOne { ContactsTable.type eq ContactType.Email }
                assertEquals("foo@bar.com", contact.text)
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }

    @Test
    fun testSelectOneManyResults() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                !ContactsSerialiserWithPersonId.selectOne { ContactsTable.personId eq 1L }
            }.mapLeft { assertEquals(DatabaseError("err-too-many-rows-found", emptyMap(), None), it); it.left() }
        }
    }

    @Test
    fun testSelectSome() {
        transaction {
            Either.fx<DatabaseError, Unit> {
                val contacts = !ContactsSerialiserWithPersonId.select { ContactsTable.personId eq 1L }
                assertEquals(2, contacts.size, "Two contacts should've been found")
            }.mapLeft { Assertions.fail<Nothing>("Unexpected failure in test case ${it} "); it.left() }
        }
    }
}