package tech.dnene.squall.sampleschemas.person

import arrow.core.Either
import arrow.core.flatMap
import arrow.core.right
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.date
import org.jetbrains.exposed.sql.statements.UpdateBuilder
import tech.dnene.squall.*
import java.time.LocalDate

enum class Sex(val str: String) {
    Male("M"),
    Female("F"),
    Unspecified("U");

    companion object {
        val map = values().map { it.str to it }.toMap()
        val fromDb = { it: Any -> map[it]!! }
        val toDb = { it: Sex -> it.str }
        val dbEnum = values().map { it.str }.joinToString("','", "ENUM('", "')")
    }
}

enum class ContactType(val str: String) {
    Mobile("M"),
    Email("E"),
    Address("A");

    companion object {
        val map = ContactType.values().map { it.str to it }.toMap()
        val fromDb = { it: Any -> map[it]!! }
        val toDb = { it: ContactType -> it.str }
        val dbEnum = values().map { it.str }.joinToString("','", "ENUM('", "')")
    }
}

data class Person(
    override val id: Long,
    val givenName: String,
    val lastName: String,
    val sex: Sex,
    val dateOfBirth: LocalDate
) : IdEntity<Long>


data class PersonContact(
    val personId: Long,
    val type: ContactType,
    val text: String
)

data class Contact(
    val type: ContactType,
    val text: String
)

data class PersonTree(
    override val id: Long,
    val givenName: String,
    val lastName: String,
    val sex: Sex,
    val dateOfBirth: LocalDate,
    val contacts: List<Contact>
) : IdEntity<Long> {
    companion object {
        fun from(p: Person, contacts: List<Contact> = emptyList()) =
            PersonTree(p.id, p.givenName, p.lastName, p.sex, p.dateOfBirth, contacts)
    }
}

object PersonsTable : IdTable<Long>("persons") {
    override val id = long("id").entityId()
    override val primaryKey = PrimaryKey(id)
    val givenName = varchar("given_name", 20)
    val lastName = varchar("last_name", 10)
    val sex = customEnumeration(
        "sex",
        Sex.dbEnum,
        Sex.fromDb,
        Sex.toDb
    )
    val dateOfBirth = date("date_of_birth")
}

object ContactsTable : Table("contacts") {
    val personId = reference("person_id", PersonsTable)
    val type = customEnumeration(
        "type",
        ContactType.dbEnum,
        ContactType.fromDb,
        ContactType.toDb
    )
    val text = varchar("text", 128)
}

object ContactsSerialiserWithPersonId : Serialiser<PersonContact, ContactsTable> {
    override val baseTable = ContactsTable
    override fun serialise(e: PersonContact): ContactsTable.(UpdateBuilder<Int>) -> Unit = {
        it[personId] = EntityID(e.personId, PersonsTable)
        it[type] = e.type
        it[text] = e.text
    }

    override fun deserialise(row: ResultRow): Either<DatabaseError, PersonContact> = PersonContact(
        row[ContactsTable.personId].value,
        row[ContactsTable.type],
        row[ContactsTable.text]
    ).right()
}

object DepContactsSerialiser : DepSerialiser<PersonTree, Contact, ContactsTable> {
    override val baseTable = ContactsTable
    override val accessor = { it: PersonTree -> it.contacts }
    override fun serialise(parent: PersonTree, e: Contact): ContactsTable.(UpdateBuilder<Int>) -> Unit = {
        it[personId] = EntityID(parent.id, PersonsTable)
        it[type] = e.type
        it[text] = e.text
    }

    override fun deserialise(row: ResultRow): Either<DatabaseError, Contact> = Contact(
        row[ContactsTable.type],
        row[ContactsTable.text]
    ).right()
}

object ContactsSerialiser : Serialiser<PersonContact, ContactsTable> {
    override val baseTable = ContactsTable
    override fun serialise(e: PersonContact): ContactsTable.(UpdateBuilder<Int>) -> Unit = {
        it[personId] = EntityID(e.personId, PersonsTable)
        it[type] = e.type
        it[text] = e.text
    }

    override fun deserialise(row: ResultRow): Either<DatabaseError, PersonContact> = PersonContact(
        row[ContactsTable.personId].value,
        row[ContactsTable.type],
        row[ContactsTable.text]
    ).right()
}

object PersonSerialiser : AbstractIdSerialiser<Long, Person, PersonsTable>() {
    override val baseTable = PersonsTable
    override fun serialise(e: Person): PersonsTable.(UpdateBuilder<Int>) -> Unit = {
        it[id] = EntityID(e.id, PersonsTable)
        it[givenName] = e.givenName
        it[lastName] = e.lastName
        it[sex] = e.sex
        it[dateOfBirth] = e.dateOfBirth
    }

    override fun deserialise(row: ResultRow): Either<DatabaseError, Person> = Person(
        row[PersonsTable.id].value,
        row[PersonsTable.givenName],
        row[PersonsTable.lastName],
        row[PersonsTable.sex],
        row[PersonsTable.dateOfBirth]
    ).right()
}


object PersonTreeSerialiser : AbstractIdSerialiser<Long, PersonTree, PersonsTable>() {
    override val baseTable = PersonsTable
    override val dependentPersisters: List<DepPersister<PersonTree, *, *>> = listOf(DepContactsSerialiser)
    override fun serialise(e: PersonTree): PersonsTable.(UpdateBuilder<Int>) -> Unit = {
        it[id] = EntityID(e.id, PersonsTable)
        it[givenName] = e.givenName
        it[lastName] = e.lastName
        it[sex] = e.sex
        it[dateOfBirth] = e.dateOfBirth
    }

    override fun deserialise(row: ResultRow): Either<DatabaseError, PersonTree> {
        val id = row[PersonsTable.id].value
        return DepContactsSerialiser.select { ContactsTable.personId eq id }.flatMap {contacts ->
            PersonTree(
                row[PersonsTable.id].value,
                row[PersonsTable.givenName],
                row[PersonsTable.lastName],
                row[PersonsTable.sex],
                row[PersonsTable.dateOfBirth],
                contacts
            ).right()
        }
    }
}