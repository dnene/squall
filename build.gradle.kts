import java.net.URI

plugins {
    kotlin("jvm") version "1.3.72"
    kotlin("kapt") version "1.3.72"
    `maven-publish`
}

group = "tech.dnene"
version = "0.0.1-SNAPSHOT"
val scmurl = "https://bitbucket.org/dnene/squall/"

repositories {
    mavenCentral()
    maven { url = URI("https://dl.bintray.com/arrow-kt/arrow-kt/") }
    maven { url = URI("https://dl.bintray.com/kotlin/exposed/") }
}

val sourcesJar by tasks.registering(Jar::class) {
    classifier = "sources"
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        create<MavenPublication>("squall") {
            groupId = "tech.dnene"
            artifactId = rootProject.name
            version = version

            from(components["java"])

            pom.withXml {
                asNode().apply {
                    appendNode("description", "Kotlin database serialisation using exposed")
                    appendNode("name", rootProject.name)
                    appendNode("url", scmurl)
                    appendNode("licenses").appendNode("license").apply {
                        appendNode("name", "Apache License V2.0")
                        appendNode("url", "https://www.apache.org/licenses/LICENSE-2.0")
                        appendNode("distribution", "repo")
                    }
                    appendNode("developers").appendNode("developer").apply {
                        appendNode("id", "dnene")
                        appendNode("name", "Dhananjay Nene")
                    }
                    appendNode("scm").apply {
                        appendNode("url", scmurl)
                    }
                }
            }
        }
    }
}


object Versions {
    val ArrowVersion = "0.10.4"
    val ExposedVersion = "0.21.1"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api("ch.qos.logback", "logback-classic", "1.2.3")
    Versions.ArrowVersion.let {
        api("io.arrow-kt", "arrow-core", it)
        api("io.arrow-kt", "arrow-syntax", it)
        kapt("io.arrow-kt", "arrow-meta", it)
    }
    Versions.ExposedVersion.let {
        api("org.jetbrains.exposed", "exposed-core", it)
        api("org.jetbrains.exposed", "exposed-jdbc", it)
        api("org.jetbrains.exposed", "exposed-java-time", it)
    }

    api("com.zaxxer", "HikariCP", "3.4.2")
    implementation("mysql", "mysql-connector-java", "8.0.19")

    testImplementation("org.junit.jupiter", "junit-jupiter", "5.6.0")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}
